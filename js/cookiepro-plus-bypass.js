/**
 * @file
 * cookiepro-plus-bypass.js
 */

(function (Drupal, once) {
  window.dataLayer = window.dataLayer || [];

  /**
   * Drupal behaviors.
   *
   * @type {{attach: Drupal.cookiepro_plus_bypass.attach}}
   */
  Drupal.behaviors.cookiepro_plus_bypass = {
    attach() {
      once('cookiepro-bypass', 'body').forEach(function () {
        window.dataLayer.push({
          event: 'OneTrustGroupsUpdated',
          OnetrustActiveGroups: 'bypass',
        });
      });
    },
  };
})(Drupal, once);
