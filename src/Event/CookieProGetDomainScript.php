<?php

namespace Drupal\cookiepro_plus\Event;

use Drupal\Core\Config\Config;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Class CookieProGetDomainScript.
 *
 * Defines the CookiePro Get Domain Script event, which allows other modules
 * to override the active CookiePro Domain Script to be used.
 */
class CookieProGetDomainScript extends Event {

  // @todo Remove when we officially drop D9 support.
  const EVENT_NAME = 'cookiepro.get_domain_script';

  /**
   * CookieProGetDomainScript constructor.
   *
   * @param string $domainScript
   *   The domain script.
   * @param \Drupal\Core\Config\Config $config
   *   The corresponding config.
   */
  public function __construct(protected string $domainScript, protected Config $config) {}

  /**
   * Get the corresponding config.
   *
   * @return \Drupal\Core\Config\Config
   *   The config.
   */
  public function getConfig(): Config {
    return $this->config;
  }

  /**
   * Get the domain script.
   *
   * @return string
   *   The domain script.
   */
  public function getDomainScript(): string {
    return $this->domainScript;
  }

  /**
   * Set the domain script.
   *
   * @param string $domain_script
   *   The domain script.
   *
   * @return \Drupal\cookiepro_plus\Event\CookieProGetDomainScript
   *   The updated event.
   */
  public function setDomainScript(string $domain_script): CookieProGetDomainScript {
    $this->domainScript = $domain_script;
    return $this;
  }

}
