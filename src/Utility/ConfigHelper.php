<?php

namespace Drupal\cookiepro_plus\Utility;

/**
 * Class with helper functions for config parsing.
 */
class ConfigHelper {

  /**
   * Convert a config array to a 'one item per line' string.
   *
   * @param string[] $array
   *   The config array.
   *
   * @return string
   *   The config string.
   */
  public static function configArrayToText(array $array): string {
    return implode("\r\n", $array);
  }

  /**
   * Convert a config string (aka 'one item per line') to an array.
   *
   * @param string $string
   *   The config as string.
   *
   * @return string[]
   *   The config as array.
   */
  public static function configTextToArray(string $string): array {
    return ($string ? explode("\r\n", trim($string)) : []);
  }

}
