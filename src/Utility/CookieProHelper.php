<?php

namespace Drupal\cookiepro_plus\Utility;

use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Path\PathMatcher;
use Drupal\Core\Routing\AdminContext;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\path_alias\AliasManager;
use GuzzleHttp\Client;
use Symfony\Component\HttpFoundation\IpUtils;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class CookieProHelper.
 *
 * The CookiePro helper.
 */
class CookieProHelper implements CookieProHelperInterface {

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Routing\AdminContext $adminContext
   *   The Admin Context.
   * @param \Drupal\path_alias\AliasManager $aliasManager
   *   The alias manager.
   * @param \GuzzleHttp\Client $httpClient
   *   The HTTP Client.
   * @param \Drupal\Core\Path\CurrentPathStack $currentPathStack
   *   The current patch stack.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The Language Manager.
   * @param \Drupal\Core\Path\PathMatcher $pathMatcher
   *   The path matcher.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $stringTranslation
   *   The string Translation Manager.
   */
  public function __construct(
    protected AdminContext $adminContext,
    protected AliasManager $aliasManager,
    protected Client $httpClient,
    protected CurrentPathStack $currentPathStack,
    protected LanguageManagerInterface $languageManager,
    protected PathMatcher $pathMatcher,
    protected TranslationInterface $stringTranslation,
  ) {}

  /**
   * {@inheritdoc}
   */
  public function isAdminRoute(): bool {
    return $this->adminContext->isAdminRoute();
  }

  /**
   * {@inheritdoc}
   */
  public function matchIp(string $ip, array $ip_list): bool {

    // Try direct match.
    $match = in_array($ip, $ip_list);

    // No match, then check ranges, if any.
    if (!$match) {
      foreach ($ip_list as $item) {

        if (strpos($item, '/') && IpUtils::checkIp($ip, $item)) {
          $match = TRUE;
          break;
        }
      }
    }

    return $match;
  }

  /**
   * {@inheritdoc}
   */
  public function matchPath(Request $request, string $path_patterns): bool {

    $path_patterns = trim($path_patterns);
    if (empty($path_patterns)) {
      return FALSE;
    }

    $path = $this->currentPathStack->getPath($request);
    $alias = $this->aliasManager->getAliasByPath($path);

    // No need for regex matching for front page.
    $path_patterns_array = ConfigHelper::configTextToArray($path_patterns);
    if ($this->pathMatcher->isFrontPage() && in_array('<front>', $path_patterns_array)) {
      return TRUE;
    }

    return $this->pathMatcher->matchPath($alias, $path_patterns);
  }

  /**
   * {@inheritdoc}
   */
  public function testUrls(array $urls): array {

    $errors = [];

    foreach ($urls as $key => $url) {
      try {
        $this->httpClient->get($url);
      }
      catch (\Exception $exception) {
        $errors[$key] = $exception->getMessage();
      }
    }

    return $errors;
  }

  /**
   * {@inheritdoc}
   */
  public function translateWithContext(string $string, array $arguments = [], string $langcode = NULL): TranslatableMarkup {

    $params = [
      'langcode' => $langcode ?? $this->languageManager->getCurrentLanguage()->getId(),
      'context' => 'cookiepro',
    ];

    return $this->stringTranslation->translate($string, $arguments, $params);
  }

}
