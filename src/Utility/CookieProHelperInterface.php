<?php

namespace Drupal\cookiepro_plus\Utility;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Symfony\Component\HttpFoundation\Request;

/**
 * Interface CookieProHelperInterface.
 *
 * Describes service to group helper functions for CookiePro.
 */
interface CookieProHelperInterface {

  /**
   * Determines whether the active route is an admin one.
   *
   * @return bool
   *   Returns TRUE if the route is an admin one, otherwise FALSE.
   */
  public function isAdminRoute(): bool;

  /**
   * Match IP with IP (range) list.
   *
   * @param string $ip
   *   The IP.
   * @param string[] $ip_list
   *   The IPs / IP ranges to match against.
   *
   * @return bool
   *   TRUE if the Request path has a match.
   */
  public function matchIp(string $ip, array $ip_list): bool;

  /**
   * Match Request with path patterns.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The Request.
   * @param string $path_patterns
   *   The path patterns.
   *
   * @return bool
   *   TRUE if the Request path has a match.
   */
  public function matchPath(Request $request, string $path_patterns): bool;

  /**
   * Test if given urls return a positive result.
   *
   * @param string[] $urls
   *   The urls to test.
   *
   * @return string[]
   *   Array of error messages.
   */
  public function testUrls(array $urls): array;

  /**
   * Convenience function to translate strings in cookiepro context.
   *
   * @param string $string
   *   A string containing the English text to translate.
   * @param array $arguments
   *   (optional) An associative array of replacements to make after
   *   translation. Based on the first character of the key, the value is
   *   escaped and/or themed. See
   *   \Drupal\Component\Render\FormattableMarkup::placeholderFormat() for
   *   details.
   * @param string|null $langcode
   *   The langcode to apply. Defaults to current interface language.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The translated label.
   */
  public function translateWithContext(string $string, array $arguments = [], string $langcode = NULL): TranslatableMarkup;

}
