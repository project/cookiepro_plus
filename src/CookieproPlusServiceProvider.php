<?php

namespace Drupal\cookiepro_plus;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Adds event subscriber if 'language_request_subscriber' service exists.
 */
class CookieproPlusServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container): void {
    $container->register('cookiepro_plus.language_request_subscriber.decorator', 'Drupal\cookiepro_plus\EventSubscriber\RequestSubscriber')
      ->setDecoratedService('language_request_subscriber', 'language_request_subscriber.inner', 10, ContainerInterface::IGNORE_ON_INVALID_REFERENCE)
      ->addArgument(new Reference('language_request_subscriber.inner'))
      ->addArgument(new Reference('cookiepro_plus'))
      ->addArgument(new Reference('language_manager'));
  }

}
