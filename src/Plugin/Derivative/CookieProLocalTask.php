<?php

namespace Drupal\cookiepro_plus\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\cookiepro_plus\CookieProConstantsInterface;
use Drupal\cookiepro_plus\CookieProInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Adds module config form local tasks for overrides by language.
 */
class CookieProLocalTask extends DeriverBase implements ContainerDeriverInterface, CookieProConstantsInterface {

  /**
   * Constructor.
   *
   * @param \Drupal\cookiepro_plus\CookieProInterface $cookiePro
   *   The CookiePro service.
   */
  public function __construct(protected CookieProInterface $cookiePro) {}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('cookiepro_plus')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition): array {
    $this->derivatives = [];

    $domains = $this->cookiePro->getUrlLanguageNegotiationDomains();
    if (!empty($domains)) {

      $weight = 1;
      foreach ($domains as $langcode => $domain) {

        $override_route = static::CONFIG_ROUTE . ".$langcode";
        $this->derivatives[$override_route] = [
          'route_name' => $override_route,
          'title' => "$domain ($langcode)",
          'base_route' => static::CONFIG_ROUTE,
          'weight' => $weight,
        ];

        $weight++;
      }

      foreach ($this->derivatives as &$entry) {
        $entry += $base_plugin_definition;
      }
    }

    return $this->derivatives;
  }

}
