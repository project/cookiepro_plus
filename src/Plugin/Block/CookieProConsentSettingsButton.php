<?php

namespace Drupal\cookiepro_plus\Plugin\Block;

/**
 * Class CookieProConsentSettingsButton.
 *
 * Provides a block with a consent settings button.
 *
 * @Block(
 *   id = "cookiepro_plus_consent_settings_button",
 *   admin_label = @Translation("Consent settings button"),
 *   category = @Translation("CookiePro"),
 * )
 */
class CookieProConsentSettingsButton extends CookieProBlockBase {

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    return [
      '#theme' => 'cookiepro_consent_settings_button',
      '#language' => $this->langcode,
    ];
  }

}
