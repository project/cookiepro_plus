<?php

namespace Drupal\cookiepro_plus\Plugin\Block;

/**
 * Class CookieProCookieList.
 *
 * Provides a cookie list block.
 *
 * @Block(
 *   id = "cookiepro_plus_cookie_list",
 *   admin_label = @Translation("Cookie List"),
 *   category = @Translation("CookiePro"),
 * )
 */
class CookieProCookieList extends CookieProBlockBase {

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    return [
      '#theme' => 'cookiepro_cookie_list',
      '#language' => $this->langcode,
    ];
  }

}
