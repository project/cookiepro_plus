<?php

namespace Drupal\cookiepro_plus\Plugin\Block;

/**
 * Class CookieProConsentSettingsLink.
 *
 * Provides a block with a consent settings link.
 *
 * @Block(
 *   id = "cookiepro_plus_consent_settings_link",
 *   admin_label = @Translation("Consent settings link"),
 *   category = @Translation("CookiePro"),
 * )
 */
class CookieProConsentSettingsLink extends CookieProBlockBase {

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    return [
      '#theme' => 'cookiepro_consent_settings_link',
      '#language' => $this->langcode,
    ];
  }

}
