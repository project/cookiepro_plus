<?php

namespace Drupal\cookiepro_plus\PageCache\RequestPolicy;

use Drupal\cookiepro_plus\CookieProInterface;
use Drupal\Core\PageCache\RequestPolicyInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class CookieProDenyCacheForWhitelistedIp.
 *
 * A policy allowing cache bypass for CookiePro whitelisted IPs.
 */
class CookieProDenyCacheForWhitelistedIp implements RequestPolicyInterface {

  /**
   * IsWhitelistedIp constructor.
   *
   * @param \Drupal\cookiepro_plus\CookieProInterface $cookiePro
   *   The CookiePro service.
   */
  public function __construct(protected CookieProInterface $cookiePro) {}

  /**
   * {@inheritdoc}
   */
  public function check(Request $request): ?string {

    if (!$this->cookiePro->isPaused() && $this->cookiePro->isWhitelistedIp($request)) {
      return static::DENY;
    }

    // No opinion.
    return NULL;
  }

}
