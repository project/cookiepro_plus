<?php

namespace Drupal\cookiepro_plus;

/**
 * Interface CookieProConstantsInterface.
 *
 * Contains all module constants.
 */
interface CookieProConstantsInterface {

  /**
   * The config key.
   *
   * @var string
   */
  const CONFIG_KEY = 'cookiepro_plus.config';

  /**
   * The 'category functional' config key.
   *
   * @var string
   */
  const CONF_CATEGORY_FUNCTIONAL = 'category_functional';

  /**
   * The 'category ids' config key.
   *
   * @var string
   */
  const CONF_CATEGORY_IDS = 'category_ids';

  /**
   * The 'category performance' config key.
   *
   * @var string
   */
  const CONF_CATEGORY_PERFORMANCE = 'category_performance';

  /**
   * The 'category social media' config key.
   *
   * @var string
   */
  const CONF_CATEGORY_SOCIAL_MEDIA = 'category_social_media';

  /**
   * The 'category strictly necessary' config key.
   *
   * @var string
   */
  const CONF_CATEGORY_STRICTLY_NECESSARY = 'category_strictly_necessary';

  /**
   * The 'category targeting' config key.
   *
   * @var string
   */
  const CONF_CATEGORY_TARGETING = 'category_targeting';

  /**
   * The 'auto block' config key.
   *
   * @var string
   */
  const CONF_AUTO_BLOCK = 'auto_block';

  /**
   * The 'document language' config key.
   *
   * @var string
   */
  const CONF_DOCUMENT_LANGUAGE = 'document_language';

  /**
   * The 'domain' config key.
   *
   * @var string
   */
  const CONF_DOMAIN = 'domain';

  /**
   * The 'domain cookiepro' config key.
   *
   * @var string
   */
  const CONF_DOMAIN_COOKIEPRO = 'domain_cookiepro';

  /**
   * The 'domain onetrust' config key.
   *
   * @var string
   */
  const CONF_DOMAIN_ONETRUST = 'domain_onetrust';

  /**
   * The 'domain script' config key.
   *
   * @var string
   */
  const CONF_DOMAIN_SCRIPT_ID = 'domain_script';

  /**
   * The 'enable limit to paths' config key.
   *
   * @var string
   */
  const CONF_ENABLE_LIMIT_TO_PATHS = 'enable_limit_to_paths';

  /**
   * The 'exclude paths' config key.
   *
   * @var string
   */
  const CONF_EXCLUDE_PATHS = 'exclude_paths';

  /**
   * The 'google consent mode enable' config key.
   *
   * @var string
   */
  const GCM_ENABLE = 'gcm_enable';

  /**
   * The 'google consent mode deny storage ad' config key.
   *
   * @var string
   */
  const GCM_DENY_STORAGES = 'gcm_deny_storages';

  /**
   * The 'ad personalization' config key.
   *
   * @var string
   */
  const GCM_DENY_AD_PERSONALIZATION = 'ad_personalization';

  /**
   * The 'ad storage' config key.
   *
   * @var string
   */
  const GCM_DENY_STORAGE_AD = 'ad_storage';

  /**
   * The 'ad user data' config key.
   *
   * @var string
   */
  const GCM_DENY_AD_USER_DATA = 'ad_user_data';

  /**
   * The 'analytics storage' config key.
   *
   * @var string
   */
  const GCM_DENY_STORAGE_ANALYTICS = 'analytics_storage';

  /**
   * The 'functionality storage' config key.
   *
   * @var string
   */
  const GCM_DENY_STORAGE_FUNCTIONALITY = 'functionality_storage';

  /**
   * The 'personalization storage' config key.
   *
   * @var string
   */
  const GCM_DENY_STORAGE_PERSONALIZATION = 'personalization_storage';

  /**
   * The 'security storage' config key.
   *
   * @var string
   */
  const GCM_DENY_STORAGE_SECURITY = 'security_storage';

  /**
   * The 'ip whitelist' config key.
   *
   * @var string
   */
  const CONF_IP_WHITELIST = 'ip_whitelist';

  /**
   * The 'limit to paths' config key.
   *
   * @var string
   */
  const CONF_LIMIT_TO_PATHS = 'limit_to_paths';

  /**
   * The 'override language' config key.
   *
   * @var string
   */
  const CONF_OVERRIDE_LANGUAGE = 'override_language';

  /**
   * The 'test cdn' config key.
   *
   * @var string
   */
  const CONF_TEST_CDN = 'test_cdn';

  /**
   * The default configuration route.
   */
  const CONFIG_ROUTE = 'cookiepro_plus.configuration';

  /**
   * The 'paused' state key.
   *
   * @var string
   */
  const STATE_PAUSED = 'cookiepro_paused';

}
