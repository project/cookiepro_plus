<?php

namespace Drupal\cookiepro_plus\EventSubscriber;

use Drupal\cookiepro_plus\CookieProConstantsInterface;
use Drupal\cookiepro_plus\CookieProInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\language\EventSubscriber\LanguageRequestSubscriber;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Overrides the active config with a language specific override, if required.
 */
class RequestSubscriber implements EventSubscriberInterface, CookieProConstantsInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    // This must run after
    // \Drupal\language\EventSubscriber\LanguageRequestSubscriber::onKernelRequestLanguage
    // which is registered with priority 255.
    $events[KernelEvents::REQUEST][] = ['onKernelRequestLanguage', 255];
    return $events;
  }

  /**
   * Constructor.
   *
   * @param \Drupal\language\EventSubscriber\LanguageRequestSubscriber $inner
   *   The language request subscriber.
   * @param \Drupal\cookiepro_plus\CookieProInterface $cookiePro
   *   The CookiePro service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The language manager service.
   */
  public function __construct(
    protected LanguageRequestSubscriber $inner,
    protected CookieProInterface $cookiePro,
    protected LanguageManagerInterface $languageManager,
  ) {}

  /**
   * Check and update the loaded config of the cookiepro_plus service.
   *
   * If URL language negotiation is enabled, there might be a config override
   * to use in the cookiepro_plus service.
   * As we instantiate the service before language negotiating has been done,
   * we have to use the default config at that time.
   * But now, after the language has been determined, we can check for and
   * load a language specific override.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   The Event to process.
   */
  public function onKernelRequestLanguage(RequestEvent $event): void {

    $this->inner->onKernelRequestLanguage($event);
    if ($event->isMainRequest() && ($this->cookiePro->getUrlLanguageNegotiationDomains())) {

      $langcode = $this->languageManager->getCurrentLanguage()->getId();
      $config = $this->cookiePro->getConfigOverride($langcode);

      if (!is_null($config)) {
        $ip_whitelist = $this->cookiePro->getIpWhitelist();

        // Set the config override and add the IP whitelist from the default.
        $this->cookiePro->setConfig($config);
        $this->cookiePro->setIpWhitelist($ip_whitelist);
      }
    }
  }

}
