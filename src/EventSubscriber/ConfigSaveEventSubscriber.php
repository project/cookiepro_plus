<?php

namespace Drupal\cookiepro_plus\EventSubscriber;

use Drupal\cookiepro_plus\CookieProInterface;
use Drupal\Core\Config\ConfigCrudEvent;
use Drupal\Core\Config\ConfigEvents;
use Drupal\Core\Menu\LocalTaskManagerInterface;
use Drupal\Core\Routing\RouteBuilderInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Refresh our config override routes and local tasks.
 */
class ConfigSaveEventSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[ConfigEvents::SAVE][] = ['onSave'];
    return $events;
  }

  /**
   * Constructor.
   *
   * @param \Drupal\cookiepro_plus\CookieProInterface $cookiePro
   *   The CookiePro service.
   * @param \Drupal\Core\Menu\LocalTaskManagerInterface $localTaskLinkManager
   *   A local task manager instance.
   * @param \Drupal\Core\Routing\RouteBuilderInterface $routeBuilder
   *   The route builder.
   */
  public function __construct(
    protected CookieProInterface $cookiePro,
    protected LocalTaskManagerInterface $localTaskLinkManager,
    protected RouteBuilderInterface $routeBuilder,
  ) {}

  /**
   * Invalidate cache tags when particular system config objects are saved.
   *
   * @param \Drupal\Core\Config\ConfigCrudEvent $event
   *   The Event to process.
   */
  public function onSave(ConfigCrudEvent $event): void {

    $config_name = $event->getConfig()->getName();
    if ($config_name === 'language.negotiation') {

      // Make sure our routes and local tasks, depending on the 'url' value, are
      // added or removed.
      if ($event->isChanged('url')) {
        $this->cookiePro->loadUrlLanguageNegotiationDomains();
        $this->routeBuilder->rebuild();
      }
      $this->localTaskLinkManager->clearCachedDefinitions();
    }
  }

}
