<?php

namespace Drupal\cookiepro_plus;

use Drupal\cookiepro_plus\Utility\CookieProHelperInterface;
use Drupal\Core\Config\Config;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Interface CookieProInterface.
 *
 * Describes the CookiePro service constants and functions.
 */
interface CookieProInterface extends CookieProConstantsInterface {

  /**
   * The CookiePro Script charset.
   *
   * @var string
   */
  const CHARSET = 'UTF-8';

  /**
   * Class to use on iframe or inline scripts for CookiePro support.
   */
  const COOKIE_CATEGORY_CLASS = 'optanon-category-%s';

  /**
   * Dummy data domain script (ID).
   *
   * @var string
   */
  const DUMMY_DATA_DOMAIN = 'a3d34529-f274-4524-aef1-fdb706f4b422';

  /**
   * Script domain for CookiePro scripts.
   *
   * @var string
   */
  const SCRIPT_DOMAIN_COOKIEPRO = 'https://cookie-cdn.cookiepro.com';

  /**
   * Script domain for OneTrust scripts.
   *
   * @var string
   */
  const SCRIPT_DOMAIN_ONETRUST = 'https://cdn.cookielaw.org';

  /**
   * The main script path.
   *
   * @var string
   */
  const SCRIPT_PATH = '/scripttemplates/otSDKStub.js';

  /**
   * The Auto-Block script path.
   *
   * @var string
   */
  const SCRIPT_PATH_AUTO_BLOCK = '/consent/%s/OtAutoBlock.js';

  /**
   * The main script JSON path.
   *
   * The JSON contains all website/domain script ID configuration.
   *
   * @var string
   */
  const SCRIPT_PATH_JSON = '/consent/%s/%s.json';

  /**
   * Add CookiePro config cache tags to given render array.
   *
   * @param array $build
   *   The build render array.
   */
  public function addConfigCacheTags(array &$build): void;

  /**
   * Disable 'pause' mode.
   *
   * @param \Drupal\Core\Config\Config|null $config
   *   The config (override) to use. When omitted, the active config will be
   *   used.
   */
  public function disablePauseMode(Config $config = NULL): void;

  /**
   * Enable 'pause' mode.
   *
   * @param \Drupal\Core\Config\Config|null $config
   *   The config (override) to use. When omitted, the active config will be
   *   used.
   */
  public function enablePauseMode(Config $config = NULL): void;

  /**
   * Returns the Auto-Block script url for current domain script.
   *
   * @param \Drupal\Core\Config\ImmutableConfig|null $config
   *   The config (override) to use. When omitted, the active config will be
   *   used.
   *
   * @return string
   *   The url.
   */
  public function getAutoBlockScriptUrl(ImmutableConfig $config = NULL): string;

  /**
   * Get class using given cookie category ID key.
   *
   * @@param string $category_key
   *   The category ID key.
   *
   * @return string
   *   The class to add based on the cookie category ID.
   */
  public function getCategoryCssClass(string $category_key): string;

  /**
   * Get Cookie Category ID labels, for forms and fields.
   *
   * @param string|null $langcode
   *   The desired translation langcode. Defaults to current interface language.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup[]
   *   The translated labels, keyed by their category config key.
   *
   * @see \Drupal\cookiepro_plus\CookieProConstantsInterface
   */
  public function getCategoryIdLabels(string $langcode = NULL): array;

  /**
   * Returns the cookie category ids.
   *
   * @return string[]
   *   The ids.
   */
  public function getCategoryIds(): array;

  /**
   * Get Cookie Category ID names, for usage in texts.
   *
   * @param string|null $langcode
   *   The desired translation langcode. Defaults to current interface language.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup[]
   *   The translated labels, keyed by their category config key.
   *
   * @see \Drupal\cookiepro_plus\CookieProConstantsInterface
   */
  public function getCategoryIdNames(string $langcode = NULL): array;

  /**
   * Returns the active config.
   *
   * @return \Drupal\Core\Config\ImmutableConfig
   *   The config.
   */
  public function getConfig(): ImmutableConfig;

  /**
   * Change the active config to the given config.
   *
   * @param \Drupal\Core\Config\ImmutableConfig $config
   *   The config.
   *
   * @return \Drupal\cookiepro_plus\CookieProInterface
   *   The updated CookiePro service.
   */
  public function setConfig(ImmutableConfig $config): CookieProInterface;

  /**
   * Get the default config.
   *
   * @return \Drupal\Core\Config\ImmutableConfig
   *   The config.
   */
  public function getConfigDefault(): ImmutableConfig;

  /**
   * Get config override for given language.
   *
   * @param string $langcode
   *   The language ID.
   *
   * @return \Drupal\Core\Config\ImmutableConfig|null
   *   The config or NULL if there is no override.
   */
  public function getConfigOverride(string $langcode): ?ImmutableConfig;

  /**
   * Get config override for given language.
   *
   * @param \Drupal\Core\Config\ImmutableConfig|null $config
   *   The config (override) to use. When omitted, the active config will be
   *   used.
   *
   * @return string
   *   The route to edit form of the configuration.
   */
  public function getConfigurationRoute(ImmutableConfig $config = NULL): string;

  /**
   * Get the CookiePro Helper.
   *
   * @return \Drupal\cookiepro_plus\Utility\CookieProHelperInterface
   *   The CookiePro Helper.
   */
  public function getCookieProHelper(): CookieProHelperInterface;

  /**
   * Get the domain to fetch the script from.
   *
   * @param \Drupal\Core\Config\Config|null $config
   *   The config (override) to use. When omitted, the active config will be
   *   used.
   *
   * @return string
   *   The domain.
   */
  public function getDomain(Config $config = NULL): string;

  /**
   * Returns the domain script ID for current mode.
   *
   * @param \Drupal\Core\Config\ImmutableConfig|null $config
   *   The config (override) to use. When omitted, the active config will be
   *   used.
   *
   * @return string
   *   The domain script.
   */
  public function getDomainScriptId(ImmutableConfig $config = NULL): string;

  /**
   * Get script to support Google Consent Mode.
   *
   * @return string|null
   *   The script to inject.
   */
  public function getGoogleConsentModeSupportScript(): ?string;

  /**
   * Get the IPv4 list.
   *
   * @return string[]
   *   The IP's.
   */
  public function getIpWhitelist(): array;

  /**
   * Change the IPv4 list in the active config.
   *
   * @param array $list
   *   The new list.
   *
   * @return \Drupal\cookiepro_plus\CookieProInterface
   *   The updated CookiePro service.
   */
  public function setIpWhitelist(array $list): CookieProInterface;

  /**
   * Get the label of the config.
   *
   * This will return 'default' or 'domain (language code)' in case domain
   * overrides.
   *
   * @param \Drupal\Core\Config\ImmutableConfig|null $config
   *   The config (override) to use. When omitted, the active config will be
   *   used.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup|null
   *   The label or NULL if domain overrides are not possible.
   */
  public function getLabel(ImmutableConfig $config = NULL): ?TranslatableMarkup;

  /**
   * Get the CookiePro logger channel.
   *
   * @return \Psr\Log\LoggerInterface
   *   The logger.
   */
  public function getLogger(): LoggerInterface;

  /**
   * Returns the main script url for the active domain.
   *
   * @return string
   *   The script url.
   */
  public function getMainScriptUrl(): string;

  /**
   * Get Pause Mode message(s).
   *
   * @param \Drupal\Core\Config\ImmutableConfig|null $config
   *   The config (override) to use. When omitted, all existing config
   *   (overrides) will be checked.
   *
   * @return array
   *   The message(s) (render) array.
   */
  public function getPauseModeMessages(ImmutableConfig $config = NULL): array;

  /**
   * Get the URL language negotiation domains.
   *
   * @return array
   *   The URL language negotiation domains keyed by the language code or NULL
   *   if not applicable.
   */
  public function getUrlLanguageNegotiationDomains(): array;

  /**
   * Returns if Auto-Blocking is enabled.
   *
   * @param \Drupal\Core\Config\ImmutableConfig|null $config
   *   The config (override) to use. When omitted, the active config will be
   *   used.
   *
   * @return bool
   *   TRUE if enabled.
   */
  public function hasAutoBlockingEnabled(ImmutableConfig $config = NULL): bool;

  /**
   * Returns if Google Consent Mode support is enabled.
   *
   * @return bool
   *   TRUE if enabled.
   */
  public function hasGoogleConsentModeEnabled(): bool;

  /**
   * Returns if language detection is enabled.
   *
   * @return bool
   *   TRUE if enabled.
   */
  public function hasLanguageDetectionEnabled(): bool;

  /**
   * Returns if Test CDN is enabled.
   *
   * @param \Drupal\Core\Config\ImmutableConfig|null $config
   *   The config (override) to use. When omitted, the active config will be
   *   used.
   *
   * @return bool
   *   TRUE if enabled.
   */
  public function hasTestCdnEnabled(ImmutableConfig $config = NULL): bool;

  /**
   * Determines whether the active route is an admin one.
   *
   * @return bool
   *   Returns TRUE if the route is an admin one, otherwise FALSE.
   */
  public function isAdminRoute(): bool;

  /**
   * Returns if the active or given config is the default.
   *
   * @param \Drupal\Core\Config\ImmutableConfig|null $config
   *   The config (override) to use. When omitted, the active config will be
   *   used.
   *
   * @return bool
   *   TRUE if the config is the default.
   */
  public function isDefaultConfig(ImmutableConfig $config = NULL): bool;

  /**
   * Check if the current path is excluded.
   *
   * @return bool
   *   TRUE if the path is excluded.
   */
  public function isExcludedPath(): bool;

  /**
   * Check if the current path is in the limited to list.
   *
   * @return bool
   *   TRUE if the path is in the limited to list, or the limited to option is
   *   not enabled.
   */
  public function isLimitedToPath(): bool;

  /**
   * Returns if the module is in 'pause' mode.
   *
   * @param \Drupal\Core\Config\Config|null $config
   *   The config (override) to use. When omitted, the active config will be
   *   used.
   *
   * @return bool
   *   TRUE if in 'pause' mode.
   */
  public function isPaused(Config $config = NULL): bool;

  /**
   * Check client IP(s) is (are) whitelisted for CookiePro bypass.
   *
   * @param \Symfony\Component\HttpFoundation\Request|null $request
   *   The request to check. Defaults to current request.
   *
   * @return bool
   *   TRUE if IP(s) is (are) whitelisted.
   */
  public function isWhitelistedIp(Request $request = NULL): bool;

  /**
   * Load the URL language negotiation domains.
   */
  public function loadUrlLanguageNegotiationDomains(): void;

  /**
   * Get if we must apply content processing for CookiePro.
   *
   * This function combines all relevant checks:
   * - isAdminRoute
   * - isWhitelistedIp
   * - isLimitedToPath
   * - isExcludedPath
   * - isPaused
   * For usage by extending/custom modules.
   *
   * @return bool
   *   TRUE if content processing must be done.
   */
  public function mustProcess(): bool;

  /**
   * Test the script urls.
   *
   * @param \Drupal\Core\Config\ImmutableConfig|null $config
   *   The config (override) to use. When omitted, the active config will be
   *   used.
   *
   * @return array
   *   Array of error messages, if any.
   */
  public function testScriptUrls(ImmutableConfig $config = NULL): array;

}
