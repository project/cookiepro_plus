<?php

namespace Drupal\cookiepro_plus;

use Drupal\cookiepro_plus\Event\CookieProGetDomainScript;
use Drupal\cookiepro_plus\Utility\ConfigHelper;
use Drupal\cookiepro_plus\Utility\CookieProHelperInterface;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\language\Plugin\LanguageNegotiation\LanguageNegotiationUrl;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * Class CookiePro.
 *
 * The main CookiePro service.
 */
class CookiePro implements CookieProInterface {

  use StringTranslationTrait;

  /**
   * Cookie category ID labels, keyed by language code.
   *
   * @var \Drupal\Core\StringTranslation\TranslatableMarkup[][]
   */
  protected array $categoryIdLabels;

  /**
   * Cookie category ID names, for no-consent messages, keyed by language code.
   *
   * @var \Drupal\Core\StringTranslation\TranslatableMarkup[][]
   */
  protected array $categoryIdNames;

  /**
   * The module default config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * IPv4 list that must bypass CookiePro.
   *
   * @var string[]|null
   */
  protected ?array $ipv4Whitelist;

  /**
   * TRUE if the current path is excluded.
   *
   * @var bool|null
   */
  protected ?bool $isExcludedPath;

  /**
   * TRUE if the current path is in the limited to list.
   *
   * @var bool|null
   */
  protected ?bool $isLimitedToPath;

  /**
   * TRUE if the client IP(s) is (are) whitelisted for CookiePro bypass.
   *
   * @var bool|null
   */
  protected ?bool $isWhitelistedIp;

  /**
   * The URL language negotiation domains, keyed by the language code.
   *
   * @var array
   */
  protected array $languageNegotiationDomains;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The configuration factory.
   * @param \Drupal\cookiepro_plus\Utility\CookieProHelperInterface $helper
   *   The CookiePro helper.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   The current request stack.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger interface.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state keyvalue collection to use.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   * @param \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $eventDispatcher
   *   The event dispatcher service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler.
   */
  public function __construct(
    protected ConfigFactoryInterface $configFactory,
    protected CookieProHelperInterface $helper,
    protected RequestStack $requestStack,
    protected LoggerInterface $logger,
    protected StateInterface $state,
    protected RendererInterface $renderer,
    protected EventDispatcherInterface $eventDispatcher,
    protected ModuleHandlerInterface $moduleHandler,
  ) {
    // Always load default config here, because when our
    // 'cookiepro_plus.deny_cache_whitelisted_ip' runs, the current user and
    // language (in case of multiple) has not been negotiated yet.
    // This also means that our IP whitelist must be a global configuration,
    // unless we want to (re)implement:
    // - \Drupal\Core\EventSubscriber\AuthenticationSubscriber::onKernelRequestAuthenticate
    // - \Drupal\language\EventSubscriber\LanguageRequestSubscriber::setLanguageOverrides
    // which we do not.
    $this->config = $this->getConfigDefault();
    $this->loadUrlLanguageNegotiationDomains();
  }

  /**
   * {@inheritdoc}
   */
  public function addConfigCacheTags(array &$build): void {
    if (!isset($build['#cache']['tags'])) {
      $build['#cache']['tags'] = [];
    }
    $build['#cache']['tags'] = Cache::mergeTags($build['#cache']['tags'], $this->config->getCacheTags());
  }

  /**
   * {@inheritdoc}
   */
  public function disablePauseMode(Config $config = NULL): void {
    $this->setPauseMode(FALSE, $config);
  }

  /**
   * {@inheritdoc}
   */
  public function enablePauseMode(Config $config = NULL): void {
    $this->setPauseMode(TRUE, $config);
  }

  /**
   * {@inheritdoc}
   */
  public function getAutoBlockScriptUrl(ImmutableConfig $config = NULL): string {

    if (is_null($config)) {
      $config = $this->config;
    }

    return $this->getDomain($config) . sprintf(static::SCRIPT_PATH_AUTO_BLOCK, $this->getDomainScriptId($config));
  }

  /**
   * {@inheritdoc}
   */
  public function getCategoryCssClass(string $category_key): string {
    $category_id = $this->getCategoryIds()[$category_key] ?? NULL;
    return $category_id ? sprintf(static::COOKIE_CATEGORY_CLASS, $category_id) : '';
  }

  /**
   * {@inheritdoc}
   */
  public function getCategoryIdLabels(string $langcode = NULL): array {

    if (!isset($this->categoryIdLabels[$langcode])) {
      $key_labels = [
        static::CONF_CATEGORY_STRICTLY_NECESSARY => 'Strictly Necessary',
        static::CONF_CATEGORY_PERFORMANCE => 'Performance',
        static::CONF_CATEGORY_FUNCTIONAL => 'Functional',
        static::CONF_CATEGORY_TARGETING => 'Targeting',
        static::CONF_CATEGORY_SOCIAL_MEDIA => 'Social Media',
      ];

      // Translate with context to prevent collision with existing translations.
      foreach ($key_labels as $key => $name) {
        $this->categoryIdLabels[$langcode][$key] = $this->helper->translateWithContext($name, [], $langcode);
      }
    }

    return $this->categoryIdLabels[$langcode];
  }

  /**
   * {@inheritdoc}
   */
  public function getCategoryIds(): array {
    return $this->config->get(static::CONF_CATEGORY_IDS);
  }

  /**
   * {@inheritdoc}
   */
  public function getCategoryIdNames(string $langcode = NULL): array {

    if (!isset($this->categoryIdNames[$langcode])) {

      $key_names = [
        static::CONF_CATEGORY_STRICTLY_NECESSARY => 'strictly necessary cookies',
        static::CONF_CATEGORY_PERFORMANCE => 'performance cookies',
        static::CONF_CATEGORY_FUNCTIONAL => 'functional cookies',
        static::CONF_CATEGORY_TARGETING => 'targeting cookies',
        static::CONF_CATEGORY_SOCIAL_MEDIA => 'social media cookies',
      ];

      // Translate with context to prevent collision with existing translations.
      foreach ($key_names as $key => $name) {
        $this->categoryIdNames[$langcode][$key] = $this->helper->translateWithContext($name, [], $langcode);
      }
    }

    return $this->categoryIdNames[$langcode];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(): ImmutableConfig {
    return $this->config;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfig(ImmutableConfig $config): CookieProInterface {
    $this->config = $config;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfigDefault(): ImmutableConfig {
    return $this->configFactory->get(static::CONFIG_KEY);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfigOverride(string $langcode): ?ImmutableConfig {

    $config = $this->configFactory->get(static::CONFIG_KEY . ".$langcode");
    // If the config does not exist, a new instance is returned.
    if (!$config->isNew()) {
      return $config;
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfigurationRoute(Config $config = NULL): string {

    if (is_null($config)) {
      $config = $this->config;
    }

    $route = static::CONFIG_ROUTE;
    if (!$this->isDefaultConfig($config)) {

      $langcode = $config->get(static::CONF_OVERRIDE_LANGUAGE);
      if (!empty($langcode)) {
        $route = "$route.$langcode";
      }
    }

    return $route;
  }

  /**
   * {@inheritdoc}
   */
  public function getCookieProHelper(): CookieProHelperInterface {
    return $this->helper;
  }

  /**
   * {@inheritdoc}
   */
  public function getDomain(Config $config = NULL): string {

    if (is_null($config)) {
      $config = $this->config;
    }

    $conf_domain = $config->get(static::CONF_DOMAIN);
    $mapping = [
      static::CONF_DOMAIN_COOKIEPRO => static::SCRIPT_DOMAIN_COOKIEPRO,
      static::CONF_DOMAIN_ONETRUST => static::SCRIPT_DOMAIN_ONETRUST,
    ];

    return $mapping[$conf_domain] ?? static::SCRIPT_DOMAIN_COOKIEPRO;
  }

  /**
   * {@inheritdoc}
   */
  public function getDomainScriptId(ImmutableConfig $config = NULL): string {

    if (is_null($config)) {
      $config = $this->config;
    }

    $domain_script = $config->get(static::CONF_DOMAIN_SCRIPT_ID);

    // Allow other modules to alter the active domain script.
    $event = new CookieProGetDomainScript($domain_script, $config);
    $this->eventDispatcher->dispatch($event);
    $domain_script = $event->getDomainScript();

    if (empty($domain_script)) {
      $this->logger->critical('Domain Script is empty.');
    }
    elseif ($this->hasTestCdnEnabled($config)) {
      $domain_script .= '-test';
    }

    return $domain_script;
  }

  /**
   * {@inheritdoc}
   */
  public function getGoogleConsentModeSupportScript(): ?string {

    if (!$this->hasGoogleConsentModeEnabled()) {
      return NULL;
    }

    $storages = $this->config->get(static::GCM_DENY_STORAGES);
    // Convert the denied storages for inclusion in inline script.
    foreach ($storages as $key => $deny) {
      if ($deny === TRUE) {
        $storages[$key] = 'denied';
      }
      else {
        unset($storages[$key]);
      }
    }

    // Set desired template.
    $script_build = [
      '#theme' => 'cookiepro_google_consent_mode',
      '#storages' => $storages,
    ];

    return $this->renderer->renderPlain($script_build);
  }

  /**
   * {@inheritdoc}
   */
  public function getIpWhitelist(): array {
    if (!isset($this->ipv4Whitelist)) {
      $this->ipv4Whitelist = ConfigHelper::configTextToArray($this->config->get(static::CONF_IP_WHITELIST));
    }

    return $this->ipv4Whitelist;
  }

  /**
   * {@inheritdoc}
   */
  public function setIpWhitelist(array $list): CookieProInterface {
    $this->ipv4Whitelist = $list;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel(ImmutableConfig $config = NULL): ?TranslatableMarkup {

    if (is_null($config)) {
      $config = $this->config;
    }

    // Using this getter might look weird for when we're not using the active
    // config, but is fine because the domains are not part of our config.
    $domains = $this->getUrlLanguageNegotiationDomains();

    if ($this->isDefaultConfig($config)) {
      return empty($domains) ? NULL : $this->t('default');
    }

    $override_language = $config->get(static::CONF_OVERRIDE_LANGUAGE);
    $params = [
      '@domain' => $domains[$override_language],
      '@langcode' => $override_language,
    ];

    return $this->t('@domain (@langcode)', $params);
  }

  /**
   * {@inheritdoc}
   */
  public function getLogger(): LoggerInterface {
    return $this->logger;
  }

  /**
   * {@inheritdoc}
   */
  public function getMainScriptUrl(): string {
    return $this->getDomain() . static::SCRIPT_PATH;
  }

  /**
   * {@inheritdoc}
   */
  public function getPauseModeMessages(ImmutableConfig $config = NULL): array {
    $configs = [];
    if (is_null($config)) {

      // Add the default config first.
      if ($this->isDefaultConfig()) {
        $configs[] = $this->config;
      }
      else {
        $configs[] = $this->getConfigDefault();
      }

      foreach ($this->getUrlLanguageNegotiationDomains() as $langcode => $domain) {

        // Reuse the active config.
        if ($this->config->getName() === static::CONFIG_KEY . ".$langcode") {
          $configs[] = $config;
          continue;
        }

        $config = $this->getConfigOverride($langcode);
        if (!is_null($config)) {
          $configs[] = $config;
        }
      }
    }
    else {
      $configs[] = $config;
    }

    $messages = [];
    foreach ($configs as $config) {
      if ($this->isPaused($config)) {

        $route = $this->getConfigurationRoute($config);
        $params = [
          ':url' => Url::fromRoute($route)->toString(),
        ];

        $label = $this->getLabel($config);
        if (!is_null($label)) {

          $params['%label'] = $label;
          $params['@label'] = $label;
          $messages[] = $this->t('CookiePro pause mode is enabled for the %label configuration. Disable at the <a href=":url" title="CookiePro @label configuration">%label</a> configuration page.', $params);
        }
        else {
          $messages[] = $this->t('CookiePro pause mode is enabled. Disable at the <a href=":url" title="CookiePro configuration">configuration</a> page.', $params);
        }
      }
    }

    return $messages;
  }

  /**
   * {@inheritdoc}
   */
  public function getUrlLanguageNegotiationDomains(): array {
    return $this->languageNegotiationDomains;
  }

  /**
   * {@inheritdoc}
   */
  public function hasAutoBlockingEnabled(ImmutableConfig $config = NULL): bool {

    if (is_null($config)) {
      $config = $this->config;
    }

    return (bool) $config->get(static::CONF_AUTO_BLOCK);
  }

  /**
   * {@inheritdoc}
   */
  public function hasGoogleConsentModeEnabled(): bool {
    return (bool) $this->config->get(static::GCM_ENABLE);
  }

  /**
   * {@inheritdoc}
   */
  public function hasLanguageDetectionEnabled(): bool {
    return (bool) $this->config->get(static::CONF_DOCUMENT_LANGUAGE);
  }

  /**
   * {@inheritdoc}
   */
  public function hasTestCdnEnabled(ImmutableConfig $config = NULL): bool {

    if (is_null($config)) {
      $config = $this->config;
    }

    return (bool) $config->get(static::CONF_TEST_CDN);
  }

  /**
   * {@inheritdoc}
   */
  public function isAdminRoute(): bool {
    return $this->helper->isAdminRoute();
  }

  /**
   * {@inheritdoc}
   */
  public function isDefaultConfig(ImmutableConfig $config = NULL): bool {

    if (is_null($config)) {
      $config = $this->config;
    }

    return ($config->getName() === static::CONFIG_KEY);
  }

  /**
   * {@inheritdoc}
   */
  public function isExcludedPath(): bool {

    if (!isset($this->isExcludedPath)) {

      $exclude_paths = $this->config->get(static::CONF_EXCLUDE_PATHS);
      $this->isExcludedPath = $this->helper->matchPath($this->requestStack->getCurrentRequest(), $exclude_paths);
    }

    return $this->isExcludedPath;
  }

  /**
   * {@inheritdoc}
   */
  public function isLimitedToPath(): bool {

    if (!isset($this->isLimitedToPath)) {

      $enabled = (bool) $this->config->get(static::CONF_ENABLE_LIMIT_TO_PATHS);
      if ($enabled === TRUE) {

        $limited_to_paths = $this->config->get(static::CONF_LIMIT_TO_PATHS);
        $this->isLimitedToPath = $this->helper->matchPath($this->requestStack->getCurrentRequest(), $limited_to_paths);
      }
      else {

        // The limited to paths option is disabled, all paths are allowed.
        $this->isLimitedToPath = TRUE;
      }
    }

    return $this->isLimitedToPath;
  }

  /**
   * {@inheritdoc}
   */
  public function isPaused(Config $config = NULL): bool {

    $state_key = $this->getStateKey($config);
    return $this->state->get($state_key, FALSE);
  }

  /**
   * {@inheritdoc}
   */
  public function isWhitelistedIp(Request $request = NULL): bool {

    $request_not_null = !is_null($request);
    if (!isset($this->isWhitelistedIp) || $request_not_null) {

      $ip = ($request_not_null) ? $request->getClientIp() : $this->requestStack->getCurrentRequest()->getClientIp();
      $match = $this->helper->matchIp($ip, $this->getIpWhitelist());

      // Don't store request specific evaluation result.
      if ($request_not_null) {
        return $match;
      }

      $this->isWhitelistedIp = $match;
    }

    return $this->isWhitelistedIp;
  }

  /**
   * {@inheritdoc}
   */
  public function loadUrlLanguageNegotiationDomains(): void {
    $this->languageNegotiationDomains = [];

    if ($this->moduleHandler->moduleExists('language')) {

      // Get language negotiation domains, if enabled for language detection.
      $config = $this->configFactory->get('language.negotiation');
      if ($config->get('url.source') === LanguageNegotiationUrl::CONFIG_DOMAIN) {

        $domains = $config->get('url.domains');
        // There is no point offering override option if there is only one
        // language/domain.
        if (is_array($domains) && count($domains) > 1) {

          foreach ($domains as &$domain) {
            if (empty($domain)) {
              $domain = $this->t('Domain missing');
            }
          }
          $this->languageNegotiationDomains = $domains;
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function mustProcess(): bool {
    return (
      !$this->isAdminRoute() &&
      !$this->isWhitelistedIp() &&
      $this->isLimitedToPath() &&
      !$this->isExcludedPath() &&
      !$this->isPaused()
    );
  }

  /**
   * {@inheritdoc}
   */
  public function testScriptUrls(ImmutableConfig $config = NULL): array {

    $script_id = $this->getDomainScriptId($config);
    $params = [
      '@environment' => $this->hasTestCdnEnabled($config) ? $this->helper->translateWithContext('Testing') : $this->helper->translateWithContext('Production'),
      '@script_id' => $script_id,
    ];

    $urls = [
      static::CONF_DOMAIN_SCRIPT_ID => $this->getDomain($config) . sprintf(static::SCRIPT_PATH_JSON, $script_id, $script_id),
    ];
    $error_messages = [
      static::CONF_DOMAIN_SCRIPT_ID => $this->helper->translateWithContext("Script for @script_id could not be loaded. Is it published to @environment CDN?", $params),
    ];

    if ($this->hasAutoBlockingEnabled($config)) {
      $urls[static::CONF_AUTO_BLOCK] = $this->getAutoBlockScriptUrl($config);
      $error_messages[static::CONF_AUTO_BLOCK] = $this->helper->translateWithContext("Auto-Blocking™ script could not be loaded. Is it enabled and published to @environment CDN?", $params);
    }

    $errors = $this->helper->testUrls($urls);
    foreach ($errors as $key => $error) {
      $context = [
        'url' => $urls[$key],
        'error' => $error,
      ];
      $this->logger->error("Error when fetching {url}: {error} ", $context);
      $errors[$key] = $error_messages[$key];
    }

    return $errors;
  }

  /**
   * Set pause mode status.
   *
   * @param bool $enabled
   *   TRUE to enable the pause modus.
   * @param \Drupal\Core\Config\Config|null $config
   *   The config (override) to use. When omitted, the currently loaded
   *   config will be used.
   */
  protected function setPauseMode(bool $enabled = FALSE, Config $config = NULL): void {

    if (is_null($config)) {
      $config = $this->config;
    }

    $state_key = $this->getStateKey($config);
    $this->state->set($state_key, $enabled);

    Cache::invalidateTags($config->getCacheTags());
  }

  /**
   * Get the state key.
   *
   * @param \Drupal\Core\Config\Config|null $config
   *   The config (override) to use. When omitted, the currently loaded
   *   config will be used.
   *
   * @return string
   *   The state key.
   */
  protected function getStateKey(Config $config = NULL): string {

    if (is_null($config)) {
      $config = $this->config;
    }

    $state_key = static::STATE_PAUSED;

    $override = $config->get(static::CONF_OVERRIDE_LANGUAGE);
    if (!empty($override)) {
      $state_key .= "_$override";
    }

    return $state_key;
  }

}
