<?php

namespace Drupal\cookiepro_plus\Routing;

use Drupal\cookiepro_plus\CookieProConstantsInterface;
use Drupal\cookiepro_plus\CookieProInterface;
use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Adds module config form routes for overrides by language.
 */
class CookieProRouteSubscriber extends RouteSubscriberBase implements CookieProConstantsInterface {

  /**
   * Constructor.
   *
   * @param \Drupal\cookiepro_plus\CookieProInterface $cookiePro
   *   The CookiePro service.
   */
  public function __construct(protected CookieProInterface $cookiePro) {}

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection): void {

    $domains = $this->cookiePro->getUrlLanguageNegotiationDomains();
    if (!empty($domains)) {

      $base_route = $collection->get(static::CONFIG_ROUTE);
      foreach ($domains as $langcode => $domain) {

        $route = new Route("{$base_route->getPath()}/$langcode");
        $route->addDefaults([
          '_title_callback' => '\Drupal\cookiepro_plus\Form\ConfigFormLanguageOverride::title',
          '_form' => '\Drupal\cookiepro_plus\Form\ConfigFormLanguageOverride',
          'langcode' => $langcode,
        ]);
        $route->addRequirements($base_route->getRequirements());

        $collection->add(static::CONFIG_ROUTE . ".$langcode", $route);
      }
    }
  }

}
