<?php

namespace Drupal\cookiepro_plus\Form;

use Drupal\cookiepro_plus\CookieProConstantsInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ConfigForm.
 *
 * Provides the default module config form.
 */
class ConfigForm extends CookieProConfigFormBase implements CookieProConstantsInterface {

  /**
   * {@inheritdoc}
   */
  protected function getConfigKey(): string {
    return static::CONFIG_KEY;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildForm($form, $form_state);

    if (!empty($this->cookiePro->getUrlLanguageNegotiationDomains())) {

      $form['language_negotiation_url'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('URL language detection with domains is enabled'),
      ];

      $message = [
        '#theme' => 'item_list',
        '#items' => [
          $this->t('Overrides per language/domain are supported.'),
          $this->t('Each override has its own Pause Mode scope.'),
          $this->t('This configuration serves as default for languages/domains without override.'),
          $this->t("Warning: Because page cache bypass is determined long before language negotiation, the 'IPv4 addresses or ranges that must bypass CookiePro' options is global and managed here."),
        ],
      ];

      $form['language_negotiation_url']['message'] = [
        '#markup' => $this->renderer->render($message),
      ];
    }

    $form[static::CONF_OVERRIDE_LANGUAGE] = [
      '#type' => 'hidden',
      '#value' => '',
    ];

    return $this->addConfigForm($form, $this->config);
  }

}
