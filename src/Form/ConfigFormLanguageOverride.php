<?php

namespace Drupal\cookiepro_plus\Form;

use Drupal\cookiepro_plus\CookieProInterface;
use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ConfigFormLanguageOverride.
 *
 * Provides the module config form for overrides by language.
 */
class ConfigFormLanguageOverride extends CookieProConfigFormBase {

  /**
   * The default module config.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected Config $configDefault;

  /**
   * The URL language negotiation domain of this override.
   *
   * @var string
   */
  protected string $domain;

  /**
   * The URL language negotiation langcode of this override.
   *
   * @var string
   */
  protected string $langcode;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    protected CookieProInterface $cookiePro,
    protected RendererInterface $renderer,
    CurrentRouteMatch $routeMatch,
  ) {
    // This needs to be first, as the parent will call $this->getConfigKey()
    // which requires the language code!
    $this->langcode = $routeMatch->getParameter('langcode');

    parent::__construct($config_factory, $cookiePro, $renderer);
    $this->routeMatch = $routeMatch;

    // Our form base always loads the default config.
    $this->configDefault = $this->config;
    $this->config = $this->config(static::CONFIG_KEY . ".$this->langcode");

    $domains = $cookiePro->getUrlLanguageNegotiationDomains();
    $this->domain = $domains[$this->langcode] ?? '';
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): CookieProConfigFormBase {
    return new static(
      $container->get('config.factory'),
      $container->get('cookiepro_plus'),
      $container->get('renderer'),
      $container->get('current_route_match')
    );
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildForm($form, $form_state);

    $has_override = !$this->config->isNew();
    $params = [
      '@domain' => $this->domain,
      '@langcode' => $this->langcode,
    ];

    $form['use_override'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use override for @domain (@langcode).', $params),
      '#default_value' => $has_override,
    ];

    $form[static::CONF_OVERRIDE_LANGUAGE] = [
      '#type' => 'hidden',
      '#value' => $this->langcode,
    ];

    $form['container'] = [
      '#type' => 'container',
      '#states' => [
        'visible' => [
          'input[name="use_override"]' => [
            'checked' => TRUE,
          ],
        ],
      ],
    ];

    // Fill the form either using existing override, or default in case of
    // creating a new override.
    $config = $has_override ? $this->config : $this->configDefault;
    $form['container'] = $this->addConfigForm($form['container'], $config);

    // We can't support this at domain level because cache bypass is determined
    // long before language negotiation.
    $form['container']['advanced'][static::CONF_IP_WHITELIST]['#type'] = 'hidden';
    $form['container']['advanced'][static::CONF_IP_WHITELIST]['#value'] = '';
    $message = [
      '#type' => 'fieldset',
      '#title' => $form['container']['advanced'][static::CONF_IP_WHITELIST]['#title'],
      'message' => [
        '#markup' => $this->t('Because page cache bypass is determined long before language negotiation, this feature can only be supported using the default configuration.'),
      ],
    ];
    array_unshift($form['container']['advanced'], $message);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function getConfigKey(): string {
    return static::CONFIG_KEY . ".$this->langcode";
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {

    if ($form_state->getValue('use_override')) {
      $form_state->unsetValue('use_override');
      parent::submitForm($form, $form_state);
    }
    elseif (!$this->config->isNew()) {
      $this->config->delete();
      $this->messenger()->addStatus($this->t('The configuration override has been deleted.'));
    }
    else {
      $this->messenger()->addStatus($this->t('Nothing to update.'));
    }
  }

  /**
   * The form page title.
   *
   * @return string
   *   The title.
   */
  public function title(): string {

    $params = [
      '@domain' => $this->domain,
      '@langcode' => $this->langcode,
    ];
    return $this->t('CookiePro for @domain (@langcode)', $params);
  }

}
