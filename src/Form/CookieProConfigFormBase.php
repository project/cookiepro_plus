<?php

namespace Drupal\cookiepro_plus\Form;

use Drupal\Component\Utility\UrlHelper;
use Drupal\cookiepro_plus\CookieProConstantsInterface;
use Drupal\cookiepro_plus\CookieProInterface;
use Drupal\cookiepro_plus\Utility\ConfigHelper;
use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\RendererInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base for module configuration forms.
 */
abstract class CookieProConfigFormBase extends ConfigFormBase implements CookieProConstantsInterface {

  /**
   * The module config.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected Config $config;

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'cookiepro_config_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      $this->getConfigKey(),
    ];
  }

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\cookiepro_plus\CookieProInterface $cookiePro
   *   The CookiePro service.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    protected CookieProInterface $cookiePro,
    protected RendererInterface $renderer,
  ) {
    parent::__construct($config_factory);
    $this->config = $this->config(static::CONFIG_KEY);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): CookieProConfigFormBase {
    return new static(
      $container->get('config.factory'),
      $container->get('cookiepro_plus'),
      $container->get('renderer')
    );
  }

  /**
   * Disable 'pause' mode.
   */
  public function disablePauseMode(): void {
    $this->cookiePro->disablePauseMode($this->config);
  }

  /**
   * Enable 'pause' mode.
   */
  public function enablePauseMode(): void {
    $this->cookiePro->enablePauseMode($this->config);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {

    $form_state->cleanValues();
    $this->config->setData($form_state->getValues())->save();

    // Test the script URLs for the updated config.
    $config = $this->configFactory()->get($this->getConfigKey());
    $errors = $this->cookiePro->testScriptUrls($config);
    foreach ($errors as $error) {
      $this->messenger()->addWarning($error);
    }

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {

    // Validate IP whitelist.
    $ip_whitelist_string = $form_state->getValue(static::CONF_IP_WHITELIST);
    $ip_whitelist_array = ConfigHelper::configTextToArray($ip_whitelist_string);

    $errors_ipv4 = [];
    $errors_ipv4_range = [];
    foreach ($ip_whitelist_array as $index => $ip) {

      // Strip leading/trailing spaces as we go.
      $ip_whitelist_array[$index] = $ip = trim($ip);

      // Make sure the IP address and netmask of a range value are valid.
      // We don't check if the range is actually possible, for now.
      if (strpos($ip, '/')) {

        [$address, $netmask] = explode('/', $ip, 2);
        if (!filter_var($address, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)
        || $netmask < 0 || $netmask > 32
        ) {
          $errors_ipv4_range[] = $ip;
        }
      }
      else {
        // Make sure IP addresses are valid.
        if (!filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
          $errors_ipv4[] = $ip;
        }
      }
    }

    $form_state->setValue(static::CONF_IP_WHITELIST, ConfigHelper::configArrayToText($ip_whitelist_array));

    // Process whitelist errors.
    $messages = [];
    if ($errors_ipv4) {
      $messages[] = $this->formatPlural(count($errors_ipv4), "@ip is not a valid IPv4 address.", "@ip are not valid IPv4 addresses.", ['@ip' => implode(', ', $errors_ipv4)]);
    }

    if ($errors_ipv4_range) {
      $messages[] = $this->formatPlural(count($errors_ipv4_range), "@ip is not a valid IPv4 range.", "@ip are not valid IPv4 ranges.", ['@ip' => implode(', ', $errors_ipv4_range)]);
    }

    if (!empty($messages)) {
      $form_state->setError($form[static::CONF_IP_WHITELIST], implode(' ', $messages));
    }

    // Group category and Google Consent Mode IDs.
    $form_state_values = $form_state->getValues();

    $category_ids = [];
    foreach ($this->cookiePro->getCategoryIdLabels() as $key => $label) {
      $category_ids[$key] = $form_state_values[$key];
      unset($form_state_values[$key]);
    }
    $form_state_values[static::CONF_CATEGORY_IDS] = $category_ids;

    $deny_storages = [];
    foreach ($this->gcmDenyStorages() as $key => $label) {
      $deny_storages[$key] = $form_state_values[$key];
      unset($form_state_values[$key]);
    }
    $form_state_values[static::GCM_DENY_STORAGES] = $deny_storages;

    $form_state->setValues($form_state_values);

    // Validate excluded and limited to paths.
    $path_input = [
      static::CONF_EXCLUDE_PATHS,
    ];

    if ($form_state->getValue(static::CONF_ENABLE_LIMIT_TO_PATHS)) {
      $path_input[] = static::CONF_LIMIT_TO_PATHS;
    }

    foreach ($path_input as $input) {
      $paths = [];
      $paths_string = $form_state->getValue($input);
      $paths_array = ConfigHelper::configTextToArray($paths_string);

      foreach ($paths_array as $key => $path) {
        $path = trim($path);

        if (!empty($path)) {
          if ($path === '<front>') {
            $paths[$key] = $path;
          }
          else {
            $paths[$key] = $this->processPathInput($path, $input, $form_state);
          }
        }
      }

      $form_state->setValue($input, ConfigHelper::configArrayToText($paths));
    }
  }

  /**
   * Add the config form fields to the form.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Config\Config $config
   *   The config to use to fill the form.
   *
   * @return array
   *   The updated form.
   *
   * @throws \Exception
   */
  protected function addConfigForm(array $form, Config $config): array {

    /*
     * Pause toggle.
     */
    $paused = $this->cookiePro->isPaused($config);
    $form['state'] = [
      '#type' => 'details',
      '#title' => $this->t('Pause mode'),
      '#description' => $this->t('Disable all module functionality. Intended for temporary usage.<br>For example, to aid in transitioning to CookiePro on existing website or in case of issues.'),
      '#open' => $paused,
    ];

    if ($paused) {
      $form['state']['disable'] = [
        '#type' => 'submit',
        '#button_type' => 'secondary',
        '#value' => $this->t('Disable'),
        '#submit' => [[$this, 'disablePauseMode']],
        '#attributes' => [
          'class' => [
            'form-item',
          ],
        ],
      ];
    }
    else {
      $form['state']['enable'] = [
        '#type' => 'submit',
        '#button_type' => 'secondary',
        '#value' => $this->t('Enable'),
        '#submit' => [[$this, 'enablePauseMode']],
        '#attributes' => [
          'class' => [
            'form-item',
          ],
        ],
      ];
    }

    $form['section'] = [
      '#type' => 'vertical_tabs',
      '#default_tab' => 'basic',
    ];

    /*
     * Basic options that should be updated/reviewed for each website.
     */
    $form['basic'] = [
      '#type' => 'details',
      '#title' => $this->t('Basic'),
      '#group' => 'section',
    ];

    $form['basic'][static::CONF_DOMAIN] = [
      '#type' => 'radios',
      '#title' => $this->t('Script domain'),
      '#description' => $this->t('CookPro and OneTrust cookie consent apps share the same functionality and scripts, but their (CDN) domain differ.'),
      '#default_value' => $config->get(static::CONF_DOMAIN) ?? static::CONF_DOMAIN_COOKIEPRO,
      '#required' => TRUE,
      '#options' => [
        static::CONF_DOMAIN_COOKIEPRO => $this->t('CookiePro'),
        static::CONF_DOMAIN_ONETRUST => $this->t('OneTrust'),
      ],
    ];

    $form['basic'][static::CONF_DOMAIN_SCRIPT_ID] = [
      '#type' => 'textfield',
      '#title' => $this->t('Script ID'),
      '#description' => $this->t('The value of the data-domain-script attribute of the main script.<br>For example "@dummy". Omit the "-test" part.', ['@dummy' => CookieProInterface::DUMMY_DATA_DOMAIN]),
      '#default_value' => $config->get(static::CONF_DOMAIN_SCRIPT_ID) ?? '',
      '#required' => TRUE,
    ];

    $form['basic'][static::CONF_CATEGORY_IDS] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Cookie Category IDs'),
      '#description' => $this->t('Cookie categories are required to support embedded iframe and script elements. Make sure these match your CookiePro category settings.'),
    ];
    $category_ids = $config->get(static::CONF_CATEGORY_IDS);
    foreach ($this->cookiePro->getCategoryIdLabels() as $key => $label) {
      $form['basic'][static::CONF_CATEGORY_IDS][$key] = [
        '#type' => 'textfield',
        '#title' => $label,
        '#default_value' => $category_ids[$key] ?? '',
        '#required' => TRUE,
        '#size' => 5,
        '#maxlength' => 5,
      ];
    }

    $form['basic'][static::CONF_TEST_CDN] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Testing CDN'),
      '#description' => $this->t('Load scripts from Testing CDN. Allows for testing on different domains and faster propagation of published changes in the CookiePro settings.'),
      '#default_value' => $config->get(static::CONF_TEST_CDN) ?? FALSE,
    ];

    $form['basic'][static::CONF_AUTO_BLOCK] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Auto-Blocking™'),
      '#description' => $this->t('This option only works if your CookiePro subscription supports this and Auto-Blocking™ is included in the published script.'),
      '#default_value' => $config->get(static::CONF_AUTO_BLOCK) ?? FALSE,
    ];

    $form['basic'][static::CONF_DOCUMENT_LANGUAGE] = [
      '#type' => 'checkbox',
      '#title' => $this->t("Enable 'Determine the language from HTML page'"),
      '#description' => $this->t('This option only works if your CookiePro subscription supports this.'),
      '#default_value' => $config->get(static::CONF_DOCUMENT_LANGUAGE) ?? FALSE,
    ];

    /*
     * Advanced options for specific cases or situations.
     */
    $form['advanced'] = [
      '#type' => 'details',
      '#title' => $this->t('Advanced'),
      '#group' => 'section',
    ];
    $form['advanced'][static::CONF_IP_WHITELIST] = [
      '#type' => 'textarea',
      '#title' => $this->t("IPv4 addresses or ranges that must bypass CookiePro"),
      '#description' => $this->t('Allows CookiePro scanner, or other tools, to view the site without CookiePro and Auto-Blocking™. Requests from these IPs will bypass page cache and the responses to be uncacheable.<br>One IP address per line.'),
      '#default_value' => $config->get(static::CONF_IP_WHITELIST) ?? '',
    ];

    $items = [
      '#theme' => 'item_list',
      '#prefix' => $this->t('Disable CookiePro script inclusion on specific paths. Admin paths are excluded by default. You can use the "*" character as a wildcard. Enter one exclusion per line using the following formats:'),
      '#items' => [
        $this->t('Use &lt;front&gt; to exclude the front page.'),
        $this->t('Use internal paths to exclude site pages. <em>Example: /about/contact</em>'),
        $this->t('Use URL parameters to further refine path matches. <em>Example: /blog?year=current</em>'),
        $this->t('Use wildcards (*) to match any part of a path. <em>Example: /shop/*/orders</em>'),
      ],
    ];

    $form['advanced'][static::CONF_EXCLUDE_PATHS] = [
      '#type' => 'textarea',
      '#title' => $this->t('Excluded paths'),
      '#description' => $this->renderer->render($items),
      '#default_value' => $config->get(static::CONF_EXCLUDE_PATHS) ?? '',
    ];

    $form['advanced'][static::CONF_ENABLE_LIMIT_TO_PATHS] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable limiting by path'),
      '#description' => $this->t('Limit loading of CookiePro to specific paths. This feature is intended to aid in transitioning to CookiePro on a production site.<br>Once CookiePro is properly setup, this option should be disabled.'),
      '#default_value' => $config->get(static::CONF_ENABLE_LIMIT_TO_PATHS) ?? FALSE,
    ];

    $items = [
      '#theme' => 'item_list',
      '#prefix' => $this->t('Limit CookiePro script inclusion to specific paths. You can use the "*" character as a wildcard. Enter one inclusion per line using the following formats:'),
      '#items' => [
        $this->t('Use &lt;front&gt; to include the front page.'),
        $this->t('Use internal paths to include site pages. <em>Example: /about/contact</em>'),
        $this->t('Use URL parameters to further refine path matches. <em>Example: /blog?year=current</em>'),
        $this->t('Use wildcards (*) to match any part of a path. <em>Example: /shop/*/orders</em>'),
      ],
    ];

    $form['advanced'][static::CONF_LIMIT_TO_PATHS] = [
      '#type' => 'textarea',
      '#title' => $this->t('Limit to paths'),
      '#description' => $this->renderer->render($items),
      '#default_value' => $config->get(static::CONF_LIMIT_TO_PATHS) ?? '',
      '#states' => [
        'visible' => [
          [':input[name="' . static::CONF_ENABLE_LIMIT_TO_PATHS . '"]' => ['checked' => TRUE]],
        ],
      ],
    ];

    /*
     * Google Consent Mode support.
     */
    $form['gcm'] = [
      '#type' => 'details',
      '#title' => $this->t('Google Consent Mode'),
      '#group' => 'section',
    ];

    $params = [
      ':url' => 'https://developers.google.com/tag-platform/security/guides/consent?consentmode=advanced#gtag.js_1',
      '@site' => 'developers.google.com',
    ];
    $form['gcm'][static::GCM_ENABLE] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Support Google Consent Mode for gtag.js'),
      '#description' => $this->t("This will add a snippet to the page to set the default consent state for gtag.js, as described on <a href=':url' title='gtag.js default consent mode implementation on @site' target='_blank'>this</a> at @site, containing the selected consent types below.<br><strong>Enabling this feature provides only one piece of the puzzle required to integrate Google Consent Mode using gtag.js! Consult Google and CookiePro/OneTrust documentation for a full guide.</strong>", $params),
      '#default_value' => $config->get(static::GCM_ENABLE) ?? FALSE,
    ];

    $form['gcm'][static::GCM_DENY_STORAGES] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Consent types'),
      '#description' => $this->t('The consent types to deny by default.'),
      '#states' => [
        'visible' => [
          [':input[name="' . static::GCM_ENABLE . '"]' => ['checked' => TRUE]],
        ],
      ],
    ];

    $deny_storages = $config->get(static::GCM_DENY_STORAGES);
    foreach ($this->gcmDenyStorages() as $key => $label) {
      $form['gcm'][static::GCM_DENY_STORAGES][$key] = [
        '#type' => 'checkbox',
        '#title' => $label,
        '#default_value' => $deny_storages[$key] ?? FALSE,
      ];
    }

    $form['#attached'] = [
      'library' => ['cookiepro_plus/cookiepro_plus.admin'],
    ];

    return $form;
  }

  /**
   * Get the config key for the form.
   *
   * @return string
   *   The key.
   */
  abstract protected function getConfigKey(): string;

  /**
   * Google Consent Mode storage labels.
   *
   * @return array
   *   Google Consent Mode storage labels keyed by their ID.
   */
  protected function gcmDenyStorages(): array {
    return [
      static::GCM_DENY_AD_PERSONALIZATION => $this->t('Ad Personalization'),
      static::GCM_DENY_STORAGE_AD => $this->t('Ad Storage'),
      static::GCM_DENY_AD_USER_DATA => $this->t('Ad User Data'),
      static::GCM_DENY_STORAGE_ANALYTICS => $this->t('Analytics Storage'),
      static::GCM_DENY_STORAGE_FUNCTIONALITY => $this->t('Functionality Storage'),
      static::GCM_DENY_STORAGE_PERSONALIZATION => $this->t('Personalization Storage'),
      static::GCM_DENY_STORAGE_SECURITY => $this->t('Security Storage'),
    ];
  }

  /**
   * Process user inputted path.
   *
   * @param string $path
   *   The path input.
   * @param string $config_key
   *   The configuration key.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return string
   *   The sanitized path.
   */
  protected function processPathInput(string $path, string $config_key, FormStateInterface $form_state): string {
    $url_parts = parse_url(trim($path));

    // Verify path is valid.
    if (!isset($url_parts['path']) || !UrlHelper::isValid($url_parts['path']) || preg_match('/^www./i', $url_parts['path'])) {
      $form_state->setErrorByName($config_key, $this->t('Invalid path: %s', ['%s' => $path]));
      return $path;
    }

    // Use parsed path in case user inputted schema and/or host.
    $path = $url_parts['path'];
    if (!empty($url_parts['query'])) {
      $path .= '?' . $url_parts['query'];
    }

    // Prepend leading forward slash.
    if (!str_starts_with($path, '/')) {
      $path = '/' . $path;
    }

    return $path;
  }

}
