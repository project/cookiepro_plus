<?php

namespace Drupal\cookiepro_plus\Asset;

use Drupal\cookiepro_plus\CookieProInterface;
use Drupal\Core\Asset\AssetCollectionRendererInterface;

/**
 * Class CookieProJsCollectionRenderer.
 *
 * Decorates \Drupal\Core\Asset\JsCollectionRenderer.
 */
class CookieProJsCollectionRenderer implements AssetCollectionRendererInterface {

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Asset\AssetCollectionRendererInterface $inner
   *   The JsCollectionRenderer service.
   * @param \Drupal\cookiepro_plus\CookieProInterface $cookiePro
   *   The CookiePro service.
   */
  public function __construct(protected AssetCollectionRendererInterface $inner, protected CookieProInterface $cookiePro) {}

  /**
   * {@inheritdoc}
   */
  public function render(array $assets): array {

    $elements = $this->inner->render($assets);
    if ($this->cookiePro->mustProcess()) {

      $category_ids = $this->cookiePro->getCategoryIds();

      try {
        foreach ($elements as $index => $element) {
          if (!empty($element['#attributes']['cookie_category'])) {

            /*
             * Script rewrite:
             * - Load the (library) script using the OneTrust.InsertScript, but
             *   let it be executed as an inline script. This circumvents the
             *   need to add all these dynamically added scripts to
             *   OptanonWrapper.
             * - Append class with category ID.
             * - Change type to plain text.
             * - Remove the src and cookie_category attributes.
             * @see https://community.cookiepro.com/s/article/UUID-730ad441-6c4d-7877-7f85-36f1e801e8ca?topicId=0TO1Q000000ssKJWAY#idm2048
             */
            $category_key = $element['#attributes']['cookie_category'];

            $element['#value'] = "OneTrust.InsertScript('{$element['#attributes']['src']}', 'body', Drupal.cookiepro_plus.attachBehaviors, null, '$category_ids[$category_key]');";
            $element['#attributes']['class'] = $this->cookiePro->getCategoryCssClass($category_key);
            $element['#attributes']['type'] = 'text/plain';

            unset($element['#attributes']['cookie_category'], $element['#attributes']['src']);

            $elements[$index] = $element;
          }
        }
      }
      catch (\Exception $exception) {
        $this->cookiePro->getLogger()->error($exception->getMessage());
      }
    }

    return $elements;
  }

}
